from flask import Flask, request, Response, url_for
import requests
import socket
import os
import setproctitle
from prometheus_client import start_http_server, Counter, Histogram
import time

# Setzen des Prozessnamens
setproctitle.setproctitle("webshop-ms")

app = Flask(__name__)

# Get prefix from environment variable
PREFIX = os.getenv('URL_PREFIX', '')
SUFFIX = os.getenv('URL_SUFFIX', '')

# Configuration for microservices
SERVICES = {
    'customer': f'http://customer{SUFFIX}:8080',
    'catalog': f'http://catalog{SUFFIX}:8080',
    'order': f'http://order{SUFFIX}:8080',
    'invoicing': f'http://invoicing{SUFFIX}:8080',
    'shipment': f'http://shipment{SUFFIX}:8080',
    'sales': f'http://sales{SUFFIX}:8080' 
}


@app.route(f'/{PREFIX}')
def index():
    hostname = socket.gethostname()
    customer_url = url_for('proxy_varied', service='customer', path='customer')
    catalog_url = url_for('proxy_varied', service='catalog', path='catalog')
    order_url = url_for('proxy_varied', service='order', path='order')
    invoicing_url = url_for('proxy_varied', service='invoicing', path='invoicing')  
    shipment_url = url_for('proxy_varied', service='shipment', path='shipment')  
    sales_url = url_for('proxy_varied', service='sales', path='sales') 
    
    return f'''
    <html>
    <head>
        <title>Auto Shop on {hostname}</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1 class="mt-4">Auto Shop on {hostname}</h1>
            <ul class="list-group">
                <li class="list-group-item"><a href="{customer_url}">Customer</a></li>
                <li class="list-group-item"><a href="{catalog_url}">Catalog</a></li>
                <li class="list-group-item"><a href="{order_url}">Order</a></li>
                <li class="list-group-item"><hr/></li>                
                <li class="list-group-item"><a href="{invoicing_url}">Invoicing</a></li> 
                <li class="list-group-item"><a href="{shipment_url}">Shipment</a></li> 
                <li class="list-group-item"><hr/></li>                
                <li class="list-group-item"><a href="{sales_url}">Sales</a></li>               
            </ul>
        </div>
    </body>
    </html>
    '''

@app.route(f'/{PREFIX}/<service>/', defaults={'path': ''}, methods=['GET', 'POST', 'PUT', 'DELETE'])
@app.route(f'/{PREFIX}/<service>/<path:path>', methods=['GET', 'POST', 'PUT', 'DELETE'])
def proxy_varied(service, path):
    if service not in SERVICES:
        return Response(f"Service {service} not found", status=404)

    # Construct the URL to forward the request to
    service_url = f"{SERVICES[service]}/{path}"

    # Forward the request to the appropriate microservice
    if request.method == 'GET':
        response = requests.get(service_url, params=request.args)
    elif request.method == 'POST':
        response = requests.post(service_url, json=request.get_json())
    elif request.method == 'PUT':
        response = requests.put(service_url, json=request.get_json())
    elif request.method == 'DELETE':
        response = requests.delete(service_url)
    else:
        return Response("Method not supported", status=405)

    # Construct a Flask Response object from the response we got from the microservice
    excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
    headers = [(name, value) for name, value in response.raw.headers.items()
               if name.lower() not in excluded_headers]

    return Response(response.content, response.status_code, headers)

@app.route('/metrics')
def metrics():
    from prometheus_client import generate_latest
    return generate_latest(), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
