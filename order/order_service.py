from flask import Flask, request, jsonify, render_template_string
import socket
import os
import requests
import setproctitle
from prometheus_client import start_http_server, Counter, Histogram
import time

# Setzen des Prozessnamens
setproctitle.setproctitle("order")

app = Flask(__name__)

orders = [
    {"id": 1, "product_id": 1, "quantity": 1, "customer_id": 1},
    {"id": 2, "product_id": 4, "quantity": 2, "customer_id": 2},
    {"id": 3, "product_id": 8, "quantity": 1, "customer_id": 3},
    {"id": 4, "product_id": 16, "quantity": 3, "customer_id": 1},
    {"id": 5, "product_id": 19, "quantity": 1, "customer_id": 2},
    {"id": 6, "product_id": 22, "quantity": 2, "customer_id": 3},
    {"id": 7, "product_id": 10, "quantity": 1, "customer_id": 1}
]

# Get environment variable(s)
SUFFIX = os.getenv('URL_SUFFIX', '')

# REST Aufrufe nach Customer und Catalog
    
CUSTOMER_SERVICE_URL = f"http://customer{SUFFIX}:8080/customer/api/"
PRODUCT_SERVICE_URL = f"http://catalog{SUFFIX}:8080/catalog/api/"

def get_customer_name(customer_id):
    response = requests.get(f"{CUSTOMER_SERVICE_URL}{customer_id}")
    if response.status_code == 200:
        return response.json().get('name', 'Unknown Customer')
    return 'Unknown Customer'

def get_product_details(product_id):
    response = requests.get(f"{PRODUCT_SERVICE_URL}{product_id}")
    if response.status_code == 200:
        product = response.json()
        return product.get('name', 'Unknown Product'), product.get('price', 0)
    return 'Unknown Product', 0

# REST-API 

@app.route('/order/api', methods=['GET'])
def get_orders():
    return jsonify(orders)

@app.route('/order/api/<int:id>', methods=['GET'])
def get_order_by_id(id):
    order = next((o for o in orders if o['id'] == id), None)
    if order:
        order['customer_name'] = get_customer_name(order['customer_id'])
        order['product_name'], order['product_price'] = get_product_details(order['product_id'])
        order['total'] = order['quantity'] * order['product_price']
        return jsonify(order)
    else:
        return jsonify({"error": "Order not found"}), 404

@app.route('/order/api/new', methods=['POST'])
def add_order():
    if not request.json or not 'product_id' in request.json or not 'quantity' in request.json or not 'customer_id' in request.json:
        return jsonify({"error": "Bad Request"}), 400
    
    new_id = max(order['id'] for order in orders) + 1 if orders else 1
    new_order = {
        "id": new_id,
        "product_id": request.json['product_id'],
        "quantity": request.json['quantity'],
        "customer_id": request.json['customer_id']
    }
    
    orders.append(new_order)
    return jsonify(new_order), 201

# HTML Ausgabe

@app.route('/order')
def order_page():
    hostname = socket.gethostname()

    enriched_orders = []
    total_all_orders = 0
    for order in orders:
        order['customer_name'] = get_customer_name(order['customer_id'])
        order['product_name'], order['product_price'] = get_product_details(order['product_id'])
        order['total'] = order['quantity'] * order['product_price']
        enriched_orders.append(order)
        total_all_orders += order['total']

    html = """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
        <title>Orders Service on {{ hostname }}</title>
    </head>
    <body>
        <div class="container">
            <h1 class="mt-5">Orders Service on {{ hostname }}</h1>
            <table class="table table-striped mt-3">
                <thead>
                    <tr>
                        <th>Order ID</th>
                        <th>Customer Name</th>
                        <th>Product Name</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    {% for order in orders %}
                    <tr>
                        <td>{{ order['id'] }}</td>
                        <td>{{ order['customer_name'] }}</td>
                        <td>{{ order['product_name'] }}</td>
                        <td>{{ order['quantity'] }}</td>
                        <td>${{ order['product_price'] }}</td>
                        <td>${{ order['total'] }}</td>
                    </tr>
                    {% endfor %}
                    <tr>
                        <td colspan="5"><strong>Total</strong></td>
                        <td><strong>${{ total_all_orders }}</strong></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
    </html>
    """
    return render_template_string(html, orders=enriched_orders, hostname=hostname, total_all_orders=total_all_orders)

@app.route('/metrics')
def metrics():
    from prometheus_client import generate_latest
    return generate_latest(), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
