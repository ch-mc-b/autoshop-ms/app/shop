from flask import Flask, jsonify, render_template_string
import socket
import setproctitle
from prometheus_client import start_http_server, Counter, Histogram
import time

# Setzen des Prozessnamens
setproctitle.setproctitle("catalog")

app = Flask(__name__)

products = [
    {"id": 1, "name": "Land Rover Range Rover", "price": 150000},
    {"id": 2, "name": "Land Rover Discovery", "price": 100000},
    {"id": 3, "name": "Land Rover Defender", "price": 120000},
    {"id": 4, "name": "BMW 3er", "price": 60000},
    {"id": 5, "name": "BMW X5", "price": 100000},
    {"id": 6, "name": "BMW i4", "price": 80000},
    {"id": 7, "name": "Audi A4", "price": 60000},
    {"id": 8, "name": "Audi Q5", "price": 70000},
    {"id": 9, "name": "Audi e-tron", "price": 100000},
    {"id": 10, "name": "Mercedes-Benz C-Klasse", "price": 70000},
    {"id": 11, "name": "Mercedes-Benz GLE", "price": 100000},
    {"id": 12, "name": "Mercedes-Benz EQS", "price": 140000},
    {"id": 13, "name": "Toyota Corolla", "price": 35000},
    {"id": 14, "name": "Toyota RAV4", "price": 45000},
    {"id": 15, "name": "Toyota Highlander", "price": 60000},
    {"id": 16, "name": "Tesla Model 3", "price": 55000},
    {"id": 17, "name": "Tesla Model Y", "price": 70000},
    {"id": 18, "name": "Tesla Model S", "price": 130000},
    {"id": 19, "name": "Ford Mustang", "price": 60000},
    {"id": 20, "name": "Ford Explorer", "price": 70000},
    {"id": 21, "name": "Ford F-150", "price": 70000},
    {"id": 22, "name": "Volkswagen Golf", "price": 45000},
    {"id": 23, "name": "Volkswagen Tiguan", "price": 50000},
    {"id": 24, "name": "Volkswagen ID.4", "price": 60000}
]

@app.route('/catalog/api', methods=['GET'])
def get_products():
    return jsonify(products)

@app.route('/catalog/api/<int:id>')
def get_product_by_id(id):
    product = next((p for p in products if p['id'] == id), None)
    if product:
        return jsonify(product)
    else:
        return jsonify({"error": "Product not found"}), 404

@app.route('/catalog', methods=['GET'])
def get_products_html():
    hostname = socket.gethostname()
    table_rows = ''.join([f'<tr><td>{product["id"]}</td><td>{product["name"]}</td><td>${product["price"]}</td></tr>' for product in products])
    return f'''
    <html>
    <head>
        <title>Catalog Service on {hostname}</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1 class="mt-4">Catalog Service on {hostname}</h1>
            <p class="lead">List of products</p>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {table_rows}
                </tbody>
            </table>
        </div>
    </body>
    </html>
    '''

@app.route('/metrics')
def metrics():
    from prometheus_client import generate_latest
    return generate_latest(), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
