from flask import Flask, jsonify, render_template_string
import socket
import setproctitle
from prometheus_client import start_http_server, Counter, Histogram
import time

# Setzen des Prozessnamens
setproctitle.setproctitle("customer")

app = Flask(__name__)

customers = [
    {"id": 1, "name": "John Doe", "email": "john.doe@example.com"},
    {"id": 2, "name": "Jane Smith", "email": "jane.smith@example.com"},
    {"id": 3, "name": "Bob Johnson", "email": "bob.johnson@example.com"}
]

@app.route('/customer/api', methods=['GET'])
def get_customers():
    return jsonify(customers)

@app.route('/customer/api/<int:id>')
def get_customer_by_id(id):
    customer = next((c for c in customers if c['id'] == id), None)
    if customer:
        return jsonify(customer)
    else:
        return jsonify({"error": "Customer not found"}), 404

@app.route('/customer', methods=['GET'])
def get_customers_html():
    hostname = socket.gethostname()
    table_rows = ''.join([f'<tr><td>{customer["id"]}</td><td>{customer["name"]}</td><td>{customer["email"]}</td></tr>' for customer in customers])
    return f'''
    <html>
    <head>
        <title>Customer Service on {hostname}</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1 class="mt-4">Customer Service on {hostname}</h1>
            <p class="lead">List of customers</p>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                    {table_rows}
                </tbody>
            </table>
        </div>
    </body>
    </html>
    '''
@app.route('/metrics')
def metrics():
    from prometheus_client import generate_latest
    return generate_latest(), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
