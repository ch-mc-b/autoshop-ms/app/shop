Autoshop
========

[[_TOC_]]

Einfacher WebShop implementiert als Microservice-Applikation.

Dies Applikation kann mehrmals gestartet werden.

Die Applikation wurde grösstenteils mit [ChatGPT](https://chatgpt.com/) erstellt. 

Um die Applikation zu starten, dieses Git Repository clonen, [Podman Desktop](https://podman-desktop.io/) installieren und innerhalb von Podman Desktop: Podman, Podman Composer aktivieren und eine PodMan Machine erstellen.

Um die Applikation von Grund auf zu bauen, einen [ChatGPT](https://chatgpt.com/) Free Account erstellen und der nachfolgenden Dokumentation 
 [hier](https://gitlab.com/ch-mc-b/autoshop-ms/edu/chatgpt) folgen.

**Images builden und Applikation ausführen**

    podman compose up --build

**Testen mittels**

* [http://localhost:8080](http://localhost:8080)   

Version 3.2.0 (Management Dashboard)
-------------

Implementierung des Management Dashboards mit Umsatzzahlen.

Version 3.1.0 (Back Office)
-------------

Implementierung der Back Office Services 
* `invoicing` - Rechnungstellung
* `shipment` - Versand

Erweiterung von `reverse_proxy` um deren Anwahl und `docker-compose.yml` zum Starten.

Version 3.0.0 (Metrics Data)
-------------

Die Services liefern nun Metric Daten für Prometheus.

**Testen mittels**

    curl localhost:8080/metrics             # Reverse Proxy / WebShop
    curl localhost:8080/catalog/metrics     # Catalog, Customer oder Order


Version 2.1.0 (Tabellarische Darstellung, Total rechnen)
-------------

Verbesserte tabellarische Darstellung, Total rechnen.

Version 2.0.3 (REST POST order)
-------------

Erstellen einer Bestellung (order) mittels REST-API

**Testen mittels**

    curl -X POST http://localhost:8080/order/order/api/new -H "Content-Type: application/json" -d '{
            "product_id": 5,
            "quantity": 3,
            "customer_id": 3
            }'

Anschliessend Seite [http://localhost:8080/order/order](http://localhost:8080/order/order) neu laden.

Version 2.0.2 (REST-Abfragen)
-------------

order holt Name Kunden und Produkt via REST-API von den anderen Services.

**Testen mittels**

* [http://localhost:8080](http://localhost:8080)

Version 2.0.1 (REST-API)
-------------

Verbessertes REST-API, gezielte Abfrage eines Datensatzes mittels Id.

**Testen mittels**

    curl http://localhost:8080/catalog/catalog/api/1
    curl http://localhost:8080/catalog/catalog/api/4

    curl http://localhost:8080/customer/customer/api/1
    curl http://localhost:8080/customer/customer/api/2

    curl http://localhost:8080/order/order/api/1
    curl http://localhost:8080/order/order/api/2    

Ohne /<id> werden alle Datensätze angezeigt.

Version 2.0.0 (verbesserte Anzeige)
-------------

Version 2.0.0 mit ansprechender Oberfläche.

**Images builden und Applikation ausführen**

    podman compose up --build

**Testen mittels**
* [http://localhost:8080](http://localhost:8080)

**Container Images in Registry abstellen**

    podman compose push

Version 1.0.0 (erste Version) 
-------------

**Images builden und Applikation ausführen**

    podman compose up --build

**Testen mittels**
* [http://localhost:8080](http://localhost:8080)

**Container Images in Registry abstellen**

    podman compose push

Neue Version erstellen
----------------------

Jede neue Version wird in einem neuen Branch erstellt, z.B.

    git checkout -b v2.0.1
    # Änderungen durchführen
    git add -A
    git commit -m "Was wurde geändert"
    git push --set-upstream origin v2.0.1

**Hinweis**: Tags in [docker-compose.yml](docker-compose.yml) sind ebenfalls nachzuführen.

Anschliessend können die Änderungen durchgeführt, getestet und lokal ins Repository abgestellt (commit) werden.

Änderungen in den `main` Branch integrieren

    git pull
    git checkout main
    git merge v2.0.1
    git push

Mehrere Instanzen
-----------------

Die Applikation und [docker-compose.yml](docker-compose.yml) sind vorbereitet um mehrere Instanzen, z.B. für mehrere Mandanten, starten zu können.

Vorgehen:
* Neues Verzeichnis erstellen, z.B. `m1`
* Datei `.env` mit den benötigten Variablen für Prefix und Port anlegen
* [docker-compose.yml](docker-compose.yml) in neues Verzeichnis kopieren

Beispiel für eine `m1/.env` Datei

    WEBSHOP_PREFIX=""
    WEBSHOP_SUFFIX="m1"    
    WEBSHOP_PORT=8081

Und zum Schluss, im neuen Verzeichnis, `podman compose up` ausführen.  

Testen mit
* [http://localhost:8081](http://localhost:8081)


**Hinweis**: der `WEBSHOP_SUFFIX` wird hinter dem Container Namen angehängt, damit nicht bestehende Container verwendet werden.    